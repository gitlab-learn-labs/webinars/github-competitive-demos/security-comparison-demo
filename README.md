### Compete Resources
Start with the recording [here](https://drive.google.com/file/d/1IBO8q7PzVykQoAPDh2Epr9zfJa3miaSK/view?usp=drive_link)  then check out the resources listed below to be able to have a conversation around why someone would choose GitLab security over GitHub. The nice part about this demo is that it uses the Node template project, so you can quickly set it up in your own group!

[GitHub Advanced Security Marketing Page](https://resources.github.com/contact/security/?utm_source=github&utm_medium=site&utm_campaign=2022q2-resources-site-ww-Security-Contact-Request&utm_content=AdvancedSecurity)

[GitLab Secure Marketing Page](https://about.gitlab.com/stages-devops-lifecycle/secure/)

[GitLab Vs GitHub 10 Reasons](https://gitlab.highspot.com/items/643870ffcba1a53530e1467f?lfrm=irel.4#1)

[GitHub Adv Security Marketing Video](https://www.youtube.com/watch?v=DA_WbNRFPT0)

[Deep Dive GitHub Security Video](https://www.youtube.com/watch?v=cgXJWaixzbg)

[Gartner Security Report](https://www.gartner.com/reviews/market/application-security-testing/compare/github-vs-gitlab)
